package com.education.cloud.system.feign.interfaces;

import com.education.cloud.system.feign.qo.WebsiteLinkQO;
import com.education.cloud.system.feign.vo.WebsiteLinkVO;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.constant.ServiceConstant;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 站点友情链接
 *
 * @author wuyun
 */
@FeignClient(name = ServiceConstant.SYSTEM_SERVICE,contextId = "websiteLinkClient")
public interface IFeignWebsiteLink {


    @RequestMapping(value = "/feign/system/websiteLink/listForPage", method = RequestMethod.POST)
    Page<WebsiteLinkVO> listForPage(@RequestBody WebsiteLinkQO qo);

    @RequestMapping(value = "/feign/system/websiteLink/save", method = RequestMethod.POST)
    int save(@RequestBody WebsiteLinkQO qo);

    @RequestMapping(value = "/feign/system/websiteLink/deleteById", method = RequestMethod.DELETE)
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/feign/system/websiteLink/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody WebsiteLinkQO qo);

    @RequestMapping(value = "/feign/system/websiteLink/get/{id}", method = RequestMethod.GET)
    WebsiteLinkVO getById(@PathVariable(value = "id") Long id);

}

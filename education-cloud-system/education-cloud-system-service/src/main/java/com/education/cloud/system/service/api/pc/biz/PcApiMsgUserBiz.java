package com.education.cloud.system.service.api.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.education.cloud.system.common.req.MsgUserDeleteREQ;
import com.education.cloud.system.common.req.MsgUserPageREQ;
import com.education.cloud.system.common.req.MsgUserViewREQ;
import com.education.cloud.system.common.resq.MsgUserPageRESQ;
import com.education.cloud.system.common.resq.MsgUserViewRESQ;
import com.education.cloud.system.service.dao.MsgUserDao;
import com.education.cloud.util.enums.ResultEnum;
import com.education.cloud.util.tools.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.aliyun.oas.utils.StringUtil;
import com.education.cloud.system.service.dao.impl.mapper.entity.MsgUser;
import com.education.cloud.system.service.dao.impl.mapper.entity.MsgUserExample;
import com.education.cloud.system.service.dao.impl.mapper.entity.MsgUserExample.Criteria;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.base.PageUtil;
import com.education.cloud.util.base.Result;

/**
 * 站内信用户记录
 *
 */
@Component
public class PcApiMsgUserBiz {

	@Autowired
	private MsgUserDao dao;

	public Result<Page<MsgUserPageRESQ>> list(MsgUserPageREQ req) {
		MsgUserExample example = new MsgUserExample();
		Criteria c = example.createCriteria();
		if (StringUtil.isNotEmpty(req.getMsgTitle())) {
			c.andMsgTitleLike(PageUtil.like(req.getMsgTitle()));
		}
		if (StringUtil.isNotEmpty(req.getMobile())) {
			c.andMobileLike(PageUtil.like(req.getMobile()));
		}
		example.setOrderByClause(" status_id desc, sort desc, id desc ");
		Page<MsgUser> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
		return Result.success(PageUtil.transform(page, MsgUserPageRESQ.class));
	}

	public Result<Integer> delete(MsgUserDeleteREQ req) {
		if (StringUtils.isEmpty(req.getId())) {
			return Result.error("ID不能为空");
		}
		int result = dao.deleteById(req.getId());
		if (result < 0) {
			return Result.error(ResultEnum.SYSTEM_DELETE_FAIL);
		}
		return Result.success(result);
	}

	public Result<MsgUserViewRESQ> view(MsgUserViewREQ req) {
		if (StringUtils.isEmpty(req.getId())) {
			return Result.error("ID不能为空");
		}
		MsgUser record = dao.getById(req.getId());
		if (ObjectUtil.isNull(record)) {
			return Result.error("找不到用户消息记录");
		}
		return Result.success(BeanUtil.copyProperties(record, MsgUserViewRESQ.class));
	}

}

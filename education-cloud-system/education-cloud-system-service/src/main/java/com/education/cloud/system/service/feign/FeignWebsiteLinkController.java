package com.education.cloud.system.service.feign;

import com.education.cloud.system.feign.interfaces.IFeignWebsiteLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.education.cloud.system.feign.qo.WebsiteLinkQO;
import com.education.cloud.system.feign.vo.WebsiteLinkVO;
import com.education.cloud.system.service.feign.biz.FeignWebsiteLinkBiz;
import com.education.cloud.util.base.BaseController;
import com.education.cloud.util.base.Page;

/**
 * 站点友情链接
 *
 * @author wuyun
 */
@RestController
public class FeignWebsiteLinkController extends BaseController implements IFeignWebsiteLink {

    @Autowired
    private FeignWebsiteLinkBiz biz;

    @Override
    public Page<WebsiteLinkVO> listForPage(@RequestBody WebsiteLinkQO qo) {
        return biz.listForPage(qo);
    }

    @Override
    public int save(@RequestBody WebsiteLinkQO qo) {
        return biz.save(qo);
    }

    @Override
    public int deleteById(@RequestBody Long id) {
        return biz.deleteById(id);
    }

    @Override
    public int updateById(@RequestBody WebsiteLinkQO qo) {
        return biz.updateById(qo);
    }

    @Override
    public WebsiteLinkVO getById(@PathVariable(value = "id") Long id) {
        return biz.getById(id);
    }

}

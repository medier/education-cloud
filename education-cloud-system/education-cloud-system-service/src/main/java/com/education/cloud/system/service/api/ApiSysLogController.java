package com.education.cloud.system.service.api;

import com.education.cloud.util.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.education.cloud.system.service.api.biz.ApiSysLogBiz;

/**
 * 后台操作日志表
 *
 * @author wujing
 */
@RestController
public class ApiSysLogController extends BaseController {

    @Autowired
    private ApiSysLogBiz biz;

}
